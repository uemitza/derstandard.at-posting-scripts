/**
 * Skript zum Filtern der gelöschten Postings auf derstandard.at
 *
 * @author ümitza (https://apps.derstandard.at/userprofil/postings/222546)
 * @version 0.0.3
 *
 * Anleitung:
 *      - Dieses Skript kopieren
 *      - Posting Übersicht (https://apps.derstandard.at/userprofil/postings) im Browser öffnen
 *      - Die Entwicklerwerkzeuge öffnen (F12 drücken)
 *      - Auf Tab "Konsole" oder "Console" wechseln
 *      - Das vorhin kopierte Skript im Eingabefeld einfügen und Enter drücken
 *
 * Getestet mit Firefox und Chrome.
 * Internet Explorer wird nicht unterstützt!!
 * Für Support dieses Posting https://derstandard.at/permalink/p/1063540493 kommentieren.
 *
 * Changelog
 *  - v0.0.3
 *      + Korrekte Anzeige der Postings (ohne ',' dazwischen)
 *      + Support URL geändert
 *  - v0.0.2
 *      + Fix: Sucher über alle Seiten (und nicht erst ab 109)
 *  - v0.0.1
 *      + Initial release
 *
 */
(function (){
	const POSTINGS_URL = 'https://apps.derstandard.at/userprofil/postings';
	const parser = new DOMParser();

	if (!window.location.href.includes(POSTINGS_URL)) {
		console.error('Nicht auf Postings Übersicht.')
		console.error(`Bitte nach ${POSTINGS_URL} navigieren.`)
		return;
	}

	function getNumberOfPages() {
		const lastPageUri = new URL(document.querySelector('.paging .last').href)
		return Number.parseInt(lastPageUri.searchParams.get('pageNumber'));
	}

	async function fetchPostings(page) {
		const response = await fetch(`${POSTINGS_URL}?pageNumber=${page}`)

		console.log(`Verarbeite Seite ${page}`);

		const html = await response.text();
		const doc = parser.parseFromString(html, 'text/html');
		return Array.from(doc.querySelectorAll('.posting'));
	}

	const numPages = getNumberOfPages();
	const requests = [];

	for (let i = 1; i <= numPages; i++) {
		requests.push(fetchPostings(i))
	}

	Promise.all(requests).then(results => {
		console.log('Postings werden gefiltert...')

		let postings = results
			.flat()
			.map(p => p.outerHTML)
			.filter(p => p.includes('status-indicator deleted'))

		console.log(`Es wurden ${postings.length} gelöschte Postings gefunden.`)

		document.querySelector('.wrap').innerHTML = postings.join('');
	}).catch(err => {
		console.error('Postings konnten nicht geladen oder gefiltert werden:', err)
		console.log('Für Support dieses Posting https://derstandard.at/permalink/p/1063540493 kommentieren')
	});

})()
